---
name: Structure
---

Our design system is broken down into the following sections:

## Foundations

Foundations represent perceptual patterns. These are less tangible aspects of the design system. Together they create a certain aesthetic for the product. For example, colors, typography, and icons.

## Components

A small design primitive. Components build upon the design foundations (typography, spacing, motion, colors, iconography, etc.) and are the reusable building blocks of the interface. For example, buttons, tabs, and input fields.

## Regions

Regions are areas formed by combining multiple components, creating a layout section that can be reused across various pages. For example, the comments and activity feed in issues or merge requests.

## Objects

[Objects](/objects/overview) capture and organize data as a single source of truth around specific tasks and user interactions. Objects are described in terms of attributes (what they contain), functions (what they do), and relationships (what they interact with). When documented as a [conceptual model](/objects/overview#conceptual-model-diagrams), objects help understand how the product works and fits together on a system level, detached from the constraints of the user interface. An effective object model tightly aligns our user's mental model with the product's data model. For example, issue, group, and [merge request](/objects/merge-request).

## Content

Content includes documentation relating to our writing style. This includes the tone and voice of the brand, as well as common grammar guidelines.

## Usability

Usability guidelines include documentation that affects the ease-of-use for different types of users. This includes accessibility and internationalization.

## Resources

Our resources section contains relevant and useful links that aide in the creation of our design system, as well as GitLab design as a whole.
